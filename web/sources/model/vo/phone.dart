library phone;
class Phone {
  int age;
  String id;
  String imageUrl;
  String name;
  String snippet;
  Phone(this.age,this.id,this.imageUrl,this.name,this.snippet);
  Phone.fromJson(Map json){
    age=json["age"];
    id=json["id"];
    imageUrl=json["imageUrl"];
    name=json["name"];
    snippet=json["snippet"];
  }
  Map toJson() {
    return {'age':age,'id': id, 'imageUrl': imageUrl, 'name': name,'snippet':snippet};
  }
}
