library app_model;
import "dart:uri";
import "dart:async";
import 'dart:html';
import 'dart:json';
import '../../../sources/model/vo/phone.dart';

import 'package:web_ui/web_ui.dart';
import 'package:web_ui/observe/observable.dart' as __observe;

class AppModel extends Observable {
  String __$view = "";
  String get view {
    if (__observe.observeReads) {
      __observe.notifyRead(this, __observe.ChangeRecord.FIELD, 'view');
    }
    return __$view;
  }
  set view(String value) {
    if (__observe.hasObservers(this)) {
      __observe.notifyChange(this, __observe.ChangeRecord.FIELD, 'view',
          __$view, value);
    }
    __$view = value;
  }
  String query="";
  List<Phone> __$phones = new List<Phone>();
  List<Phone> get phones {
    if (__observe.observeReads) {
      __observe.notifyRead(this, __observe.ChangeRecord.FIELD, 'phones');
    }
    return __$phones;
  }
  set phones(List<Phone> value) {
    if (__observe.hasObservers(this)) {
      __observe.notifyChange(this, __observe.ChangeRecord.FIELD, 'phones',
          __$phones, value);
    }
    __$phones = value;
  }


  void load(String url){
    var request = HttpRequest.getString(url).then((String response){
      parse(response).forEach((v)=>phones.add(new Phone.fromJson(v)));

    });
  }
  List<Phone> get filteredPhones{
    var lQuery=query.toLowerCase();
    var result=phones.where((phone)=>phone.name.toLowerCase().contains(lQuery));
    print(result.toList().length);
    return result.toList();
  }
  bool get noMatches => filteredPhones.isEmpty;
  bool get isLista => view=="lista" && phones.length>0;
  bool get isDetails => view=="details"&& phones.length>0;

}


//@ sourceMappingURL=appmodel.dart.map