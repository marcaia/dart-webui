library urls;
import 'package:route/url_pattern.dart';
import "dart:html" as html;
final UrlPattern searchUrl = new UrlPattern(html.window.location.pathname);
final UrlPattern detailsUrl=new UrlPattern(html.window.location.pathname+r'#/phones/(\S+)');


final allUrls = [searchUrl, detailsUrl];

