library example;

import 'dart:html';
import 'package:route/client.dart';

import "dart:uri";
import "dart:async";
import 'package:json_object/json_object.dart';

final one = new UrlPattern(r'/one');
final two = new UrlPattern(r'/two');

main() {
  query('#warning').remove();
  query('#one').classes.add('selected');

 /* var router = new Router(useFragment:null)
    ..addHandler(one, showOne)
    ..addHandler(two, showTwo)
    ..listen();*/
  var request = HttpRequest.getString("phones/motorola-xoom-with-wi-fi.json").then((String response){
   var phone = new JsonObject.fromJsonString(response);
   print("${phone.images}");
  });
}

void showOne(String path) {
  print("showOne");
  query('#one').classes.add('selected');
  query('#two').classes.remove('selected');
}

void showTwo(String path) {
  print("showTwo");
  query('#one').classes.remove('selected');
  query('#two').classes.add('selected');
}
