library app_catalog;

import 'package:web_ui/web_ui.dart';
import 'dart:html';

import "model/vo/phone.dart";
import "dart:uri";
import "dart:async";
import 'dart:html';
import 'dart:json';
import 'package:json_object/json_object.dart';
//import 'package:web_ui/watcher.dart' as watchers;

import 'package:route/client.dart';

import "model/vo/phone.dart";

import "model/urls.dart";
import "model/appmodel.dart";




class AppCatalog extends WebComponent{
  final AppModel model=new AppModel();
  final Router  router= new Router (useFragment:null);

  void created(){

    router.addHandler(searchUrl, showSearch);
    router.addHandler(detailsUrl, showDetails);
    router.listen();

  }
  void insered(){

  }
  set phonesurl(String value){
    model.load(value);

  }
  void showSearch(String path){
    model.view="lista";

  }
  void showDetails(path){
    model.view="details";
  }
}