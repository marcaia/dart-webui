library app_model;
import "dart:uri";
import "dart:async";
import 'dart:html';
import 'dart:json';
import "vo/phone.dart";

import 'package:web_ui/web_ui.dart';
class AppModel{
  @observable
  String view="";
  String query="";
  @observable
  List<Phone> phones=new List<Phone>();


  void load(String url){
    var request = HttpRequest.getString(url).then((String response){
      parse(response).forEach((v)=>phones.add(new Phone.fromJson(v)));

    });
  }
  List<Phone> get filteredPhones{
    var lQuery=query.toLowerCase();
    var result=phones.where((phone)=>phone.name.toLowerCase().contains(lQuery));
    print(result.toList().length);
    return result.toList();
  }
  bool get noMatches => filteredPhones.isEmpty;
  bool get isLista => view=="lista" && phones.length>0;
  bool get isDetails => view=="details"&& phones.length>0;

}

