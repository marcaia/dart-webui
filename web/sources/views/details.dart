library details;
import 'package:web_ui/web_ui.dart';
import "../model/vo/phone.dart";
import "dart:uri";
import "dart:async";
import 'dart:html';
import 'dart:json';
import 'package:json_object/json_object.dart';

class DetailsComponent extends WebComponent{
  DetailModel model=new DetailModel();


  void created(){
    var uri = new Uri(window.location.toString());

    var product= uri.fragment.replaceFirst("/","")+".json";
    model.load(product);
  }
  void changeImage(int value){
    model.currentImageIndex=value;
  }

}


class DetailModel{
  @observable
  JsonObject phone;
  Timer timer;
  @observable
  int currentImageIndex=0;
  void load(String url){
    var request = HttpRequest.getString(url).then(onDataLoaded);
  }
  void onDataLoaded(String responseText) {
    phone = new JsonObject.fromJsonString(responseText);


  }
  bool get loaded=>phone!=null;
  get currentImage=>phone.images[currentImageIndex];
}
